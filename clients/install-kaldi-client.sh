#!/bin/bash

set -e

CLIENTS=$(dirname $0)

git clone https://github.com/daanzu/deepspeech-websocket-server $CLIENTS/kaldi
cd $CLIENTS/kaldi
pip3 install -r requirements-client.txt

cat > client.patch << EOF
8a9
> from urllib.parse import urlencode
224c225,226
<     websocket = WebSocket(ARGS.server)
---
>     content_type = "audio/x-raw, layout=(string)interleaved, rate=(int)%d, format=(string)S16LE, channels=(int)1" %(Audio.RATE)
>     websocket = WebSocket(ARGS.server + "?" + urlencode({"content-type": content_type}))
256,257c258,259
<     parser.add_argument('-s', '--server', default='ws://localhost:8080/recognize',
<         help="Default: ws://localhost:8080/recognize")
---
>     parser.add_argument('-s', '--server', default='ws://localhost:8080/client/ws/speech',
>         help="Default: ws://localhost:8080/client/ws/speech")
EOF

patch client.py client.patch
