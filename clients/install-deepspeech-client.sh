#!/bin/bash

set -e

CLIENTS=$(dirname $0)

git clone https://github.com/daanzu/deepspeech-websocket-server $CLIENTS/deepspeech
cd $CLIENTS/deepspeech
pip3 install -r requirements-client.txt
