#!/bin/bash

set -e

SERVERS=$(dirname $0)

docker build -t bertag/deepspeech-http $SERVERS/deepspeech-http
docker build -t bertag/deepspeech-ws $SERVERS/deepspeech-ws
docker build -t bertag/kaldi $SERVERS/kaldi

docker run --name deepspeech-http --rm --detach --publish 10101:8080 bertag/deepspeech-http:latest
docker run --name deepspeech-ws --rm --detach --publish 10102:8080 bertag/deepspeech-ws:latest
docker run --name kaldi --rm --detach --publish 10103:80 bertag/kaldi:latest
