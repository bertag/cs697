#!/bin/bash

CORPORA=$(dirname $0)

mkdir -p $CORPORA/staeds
cd $CORPORA/staeds
curl --location http://www.openslr.org/resources/45/ST-AEDS-20180100_1-OS.tgz | tar -xz
