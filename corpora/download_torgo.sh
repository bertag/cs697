#!/bin/bash

CORPORA=$(dirname $0)

for VARIANT in F FC M MC; do
  mkdir -p $CORPORA/torgo/$VARIANT
  cd $CORPORA/torgo/$VARIANT
  curl --location http://www.cs.toronto.edu/~complingweb/data/TORGO/$VARIANT.tar.bz2 | tar -xj
  cd -
done
