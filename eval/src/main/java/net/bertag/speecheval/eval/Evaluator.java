package net.bertag.speecheval.eval;

import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToDoubleBiFunction;
import java.util.stream.Collectors;
import net.bertag.speecheval.clients.SttClient;
import net.bertag.speecheval.clients.SttClientException;
import net.bertag.speecheval.data.Dataset;

/**
 * Evaluates the effectiveness of an ASR system against a particular speech corpus.
 */
public class Evaluator {

  private SttClient client;
  private Map<String, ToDoubleBiFunction<String, String>> scoring;

  /**
   * Constructs a new instance.
   * 
   * @param client the speech-to-text client of the ASR system being evaluated
   * @param scoring the scoring method(s) which should be applied
   */
  public Evaluator(SttClient client, Map<String, ToDoubleBiFunction<String, String>> scoring) {
    this.client = client;
    this.scoring = scoring;
  }

  /**
   * Evaluates all utterances in the provided dataset. 
   * 
   * @param dataset the dataset to evaluate against
   * @return the scores
   */
  public Map<String, Double> evaluate(Dataset dataset) {
    Scores scores = new Scores(dataset, scoring);
    dataset.instances().forEach((k, v) -> {
      evaluate(k, v, scores);
      scores.printStatus();
    });
    System.out.println();

    return scores.average();
  }

  /**
   * Evaluates all utterances in the provided dataset asynchronously. 
   * 
   * @param dataset the dataset to evaluate against
   * @return the scores
   */
  public Map<String, Double> evaluateAsync(Dataset dataset, int threads) {
    AtomicBoolean done = new AtomicBoolean(false);
    Scores scores = new Scores(dataset, scoring);

    // Test all the samples in the dataset.
    ExecutorService exec = Executors.newFixedThreadPool(threads);
    dataset.instances().forEach((k, v) -> exec.submit(() -> evaluate(k, v, scores)));

    // Update the status bar every second.
    Thread statusBarThread = new Thread(() -> {
      while (!done.get()) {
        scores.printStatus();
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          break;
        }
      }
    });
    statusBarThread.start();

    exec.shutdown();
    try {
      exec.awaitTermination(1, TimeUnit.DAYS);
    } catch (InterruptedException e) {
      System.err.println("Evaluation was interrupted.");
    }
    done.set(true);
    statusBarThread.interrupt();

    // Update the status bar a final time.
    scores.printStatus();
    System.out.println();

    // Return the average scores.
    return scores.average();
  }

  /**
   * Internal method for evaluating the accuracy of a single utterance.
   * 
   * @param soundFile the path to the sound file being tested
   * @param reference the "ground truth" reference text of the utterance
   * @param scores pointer to the scores which should be updated
   */
  private void evaluate(Path soundFile, String reference, Scores scores) {
    try {
      scores.addScores(evaluate(soundFile, reference));
    } catch (SttClientException | IllegalArgumentException e) {
      // If we fail on a particular sample, just log it and move on.
      System.err.println(String.format("%s thrown while evaluating %s: %s",
          e.getClass().getSimpleName(), soundFile, e));
    }
  }

  /**
   * Evaluates the accuracy of a single utterance.
   * 
   * @param soundFile the path to the sound file being tested
   * @param reference the "ground truth" reference text of the utterance
   */
  public Map<String, Double> evaluate(Path wavPath, String reference)
      throws SttClientException, IllegalArgumentException {
    String hypothesis = client.stt(wavPath);
    return scoring.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey(),
        entry -> entry.getValue().applyAsDouble(reference, hypothesis)));
  }

  /**
   * Helper class used to keep track of and update scores in a thread-safe manner.
   */
  public static class Scores {

    private final StatusBar statusBar;
    private final Map<String, DoubleAdder> scores;
    private final AtomicInteger count = new AtomicInteger();
    private final Supplier<String> latestStatus = () -> average().entrySet().stream()
          .map(entry -> String.format("%s: %.2f", entry.getKey(), entry.getValue()))
          .collect(Collectors.joining(" | "));

    /**
     * Constructs a new instance.
     * 
     * @param dataset the dataset to evaluate against
     * @param scoring the scoring method(s) which should be applied
     */
    public Scores(Dataset dataset, Map<String, ToDoubleBiFunction<String, String>> scoring) {
      statusBar = new StatusBar(dataset.instances().size());
      this.scores = scoring.keySet().stream().collect(Collectors.toMap(Function.identity(),
          label -> new DoubleAdder(), (a, b) -> a, ConcurrentHashMap::new));
    }

    /**
     * Updates the overall scores with the given values.
     * 
     * @param newScores the new scores to incorporate
     */
    public void addScores(Map<String, Double> newScores) {
      newScores.forEach((label, score) -> scores.get(label).add(score));
      count.incrementAndGet();
    }

    /**
     * Prints a status bar showing the progress of the evaluation pass.
     */
    public void printStatus() {
      statusBar.print(count.get(), latestStatus);
    }

    /**
     * Takes the average of each type of score.
     * 
     * @return the average score(s)
     */
    public Map<String, Double> average() {
      return scores.entrySet().stream().collect(
          Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().sum() / count.get()));
    }

  }

}
