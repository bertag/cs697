package net.bertag.speecheval.eval;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.function.Supplier;

/**
 * Since the evaluation may take a long time, this status bar can be used to show incremental
 * progress and scores.
 */
public class StatusBar {

  private static final int WIDTH = 20;
  private final int total;

  /**
   * Constructs a new instance.
   * 
   * @param total the total number of utterances being evaluated
   */
  public StatusBar(int total) {
    this.total = total;
  }

  /**
   * Prints an updated status bar to the console.
   * 
   * @param current the current index of the iteration between zero and {@code total}.
   * @param messageSupplier the message which should be printed with the status bar
   */
  public void print(int current, Supplier<String> messageSupplier) {
    print(current, messageSupplier.get());
  }

  /**
   * Prints an updated status bar to the console.
   * 
   * @param current the current index of the iteration between zero and {@code total}.
   * @param messageSupplier the message which should be printed with the status bar
   */
  public void print(int current, String message) {
    int filled = (int) Math.round((double) current / total * WIDTH);
    int blank = WIDTH - filled;

    String filledStr = String.join("", Collections.nCopies(filled, "="));
    String blankStr = String.join("", Collections.nCopies(blank, " "));
    String progress = current + "/" + total;

    System.out.print(String.format("%s [%s%s] %s | %s\r",
        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS), filledStr, blankStr, progress, message));
  }

}
