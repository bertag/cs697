package net.bertag.speecheval.eval;

import com.pwnetics.metric.WordSequenceAligner;
import com.pwnetics.metric.WordSequenceAligner.Alignment;
import java.util.Map;
import java.util.Objects;
import java.util.function.ToDoubleBiFunction;
import org.apache.commons.text.similarity.CosineDistance;
import org.apache.commons.text.similarity.LevenshteinDistance;

/** Defines scoring methodology for the evaluation. */
public class Scoring {

  /**
   * All known scoring metrics, viewed as a Map where the key is the name of the score, and the
   * value is a lambda expression defining how the score is computed.
   */
  public static Map<String, ToDoubleBiFunction<String, String>> ALL =
      Map.of(
          "equality",
          Scoring::equality,
          "levenstein",
          Scoring::levenshtein,
          "cosine",
          Scoring::cosine,
          "wer",
          Scoring::wer);

  private static LevenshteinDistance levenshtein = new LevenshteinDistance();
  private static CosineDistance cosine = new CosineDistance();
  private static WordSequenceAligner wer = new WordSequenceAligner();

  /**
   * Simple equality score -- if the predicted string is strictly equal to the actual "ground truth"
   * string, the prediction scores 1; otherwise the prediction scores 0.
   *
   * <p>This score is bounded between 0-1; higher is better.
   *
   * @param reference the expected "ground truth" string
   * @param hypothesis the predicted or learned string returned by the ASR system
   * @return the score
   */
  public static double equality(String reference, String hypothesis) {
    return Objects.equals(reference, hypothesis) ? 1 : 0;
  }

  /**
   * The Levenshtein distance between the predicted and actual "ground truth" string.
   *
   * <p>This score is bounded between 0-∞; lower is better.
   *
   * @param reference the expected "ground truth" string
   * @param hypothesis the predicted or learned string returned by the ASR system
   * @return the score
   */
  public static double levenshtein(String reference, String hypothesis) {
    return levenshtein.apply(reference, hypothesis);
  }

  /**
   * The Cosine distance between the predicted and actual "ground truth" string.
   *
   * <p>This score is bounded between 0-∞; lower is better.
   *
   * @param reference the expected "ground truth" string
   * @param hypothesis the predicted or learned string returned by the ASR system
   * @return the score
   */
  public static double cosine(String reference, String hypothesis) {
    return cosine.apply(reference, hypothesis);
  }

  /**
   * The word error rate (WER) between the predicted and actual "ground truth" string.
   *
   * <p>This score is bounded between 0-∞; lower is better.
   *
   * @param reference the expected "ground truth" string
   * @param hypothesis the predicted or learned string returned by the ASR system
   * @return the score
   */
  public static double wer(String reference, String hypothesis) {
    Alignment alignment = wer.align(reference.split("\\s+"), hypothesis.split("\\s+"));

    if (alignment.getReferenceLength() < 1) {
      // Make sure we avoid division by zero by doing a boolean compare.
      return alignment.getReferenceLength() == alignment.getHypothesisLength() ? 0 : 1;
    } else {
      int delta = alignment.numSubstitutions + alignment.numDeletions + alignment.numInsertions;
      return (double) delta / alignment.getReferenceLength();
    }
  }

  /** This is a utility class; suppress the default constructor. */
  private Scoring() {
    // Do nothing.
  }
}
