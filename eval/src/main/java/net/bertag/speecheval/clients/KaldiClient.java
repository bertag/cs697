package net.bertag.speecheval.clients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.byu.hbll.json.ObjectMapperFactory;
import edu.byu.hbll.misc.Streams;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Path;
import net.bertag.speecheval.Utils;

/** Client for the Kaldi gstreamer server implementation over HTTP. */
public class KaldiClient implements SttClient {

  private static final ObjectMapper objectMapper = ObjectMapperFactory.newChecked();

  private static final HttpClient client =
      HttpClient.newBuilder().version(Version.HTTP_1_1).build();

  private final URI uri;

  /**
   * Constructs a new instance.
   *
   * @param uri the URI to the deep speech service (e.g.: localhost:8080/stt)
   */
  public KaldiClient(URI uri) {
    this.uri = uri;
  }

  @Override
  public String stt(Path soundFile) throws SttClientException {
    try {
      byte[] bytes = Files.readAllBytes(soundFile);
      HttpRequest request =
          HttpRequest.newBuilder(uri).PUT(BodyPublishers.ofByteArray(bytes)).build();
      HttpResponse<byte[]> response = client.send(request, BodyHandlers.ofByteArray());

      // The Kaldi server returns JSON. Parse out the hypothesis utterance and normalize.
      JsonNode json = objectMapper.readTree(response.body());
      String hypothesis =
          Streams.of(json.path("hypotheses"))
              .map(node -> node.path("utterance").asText(""))
              .findAny()
              .orElse("");
      return Utils.normalize(hypothesis);
    } catch (IOException | InterruptedException | IllegalArgumentException e) {
      throw new SttClientException(e);
    }
  }
}
