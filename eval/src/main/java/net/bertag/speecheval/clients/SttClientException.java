package net.bertag.speecheval.clients;

/**
 * Standard exception for any problems encountered while running {@link
 * SttClient#stt(java.nio.file.Path)}.
 */
public class SttClientException extends Exception {

  private static final long serialVersionUID = 1L;

  public SttClientException() {
    super();
  }

  public SttClientException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public SttClientException(String message, Throwable cause) {
    super(message, cause);
  }

  public SttClientException(String message) {
    super(message);
  }

  public SttClientException(Throwable cause) {
    super(cause);
  }
}
