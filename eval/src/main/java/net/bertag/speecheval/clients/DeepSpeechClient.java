package net.bertag.speecheval.clients;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Path;
import net.bertag.speecheval.Utils;

/**
 * Client for the Mozilla Deep Speech implementation over HTTP.
 */
public class DeepSpeechClient implements SttClient {

  private static final HttpClient client =
      HttpClient.newBuilder().version(Version.HTTP_1_1).build();

  private final URI uri;

  /**
   * Constructs a new instance.
   * 
   * @param uri the URI to the deep speech service (e.g.: localhost:8080/stt)
   */
  public DeepSpeechClient(URI uri) {
    this.uri = uri;
  }

  @Override
  public String stt(Path soundFile) throws SttClientException {
    try {
      byte[] bytes = Files.readAllBytes(soundFile);
      HttpRequest request =
          HttpRequest.newBuilder(uri).POST(BodyPublishers.ofByteArray(bytes)).build();
      HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
      return Utils.normalize(response.body());
    } catch (IOException | InterruptedException | IllegalArgumentException e) {
      throw new SttClientException(e);
    }
  }
}
