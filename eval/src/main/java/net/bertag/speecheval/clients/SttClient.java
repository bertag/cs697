package net.bertag.speecheval.clients;

import java.nio.file.Path;

/**
 * Interface defining a client for interacting with a speech-to-text (STT) service.  Classes
 * implementing this interface MUST define a constructor which accepts a {@link URI} pointing to the
 * service.
 */
public interface SttClient {

  /**
   * Calls the speech-to-text service and returns the resulting transcription.
   * 
   * @param soundFile the path to the .wav file which should be sent to the service
   */
  public String stt(Path soundFile) throws SttClientException;

}
