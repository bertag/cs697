package net.bertag.speecheval;

/**
 * Utility methods used by several other classes.
 */
public class Utils {

  /**
   * Normalizes a string by stripping off all punctuation.
   * 
   * @param text the string to normalize
   * @return the normalized string
   */
  public static String normalize(String text) {
    return text.trim().toLowerCase().replaceAll("[^A-Za-z0-9 ]", "");
  }

}
