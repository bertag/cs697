package net.bertag.speecheval;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import net.bertag.speecheval.clients.SttClient;
import net.bertag.speecheval.data.Dataset;
import net.bertag.speecheval.data.DatasetBuilder;
import net.bertag.speecheval.eval.Evaluator;
import net.bertag.speecheval.eval.Scoring;

/** Main runtime. */
public class Main {

  private static final String USAGE =
      "Usage: java -jar speecheval.jar CLIENT_CLASS ASR_URL DATASET_BUILDER_CLASS DATASET_PATH [SAMPLE_RATIO]\n"
          + "  CLIENT_CLASS: fully qualified name of the class used to query the ASR system\n"
          + "  ASR_URL:      the url where the ASR system can be queried\n"
          + "  DATASET_BUILDER_CLASS: fully qualified name of the class that will be used to parse the data\n"
          + "  DATASET_PATH: directory path to the dataset\n"
          + "  SAMPLE_RATIO: optional number between 0 and 1 used to randomly sample instances\n"
          + "                from the dataset (e.g.: 0.1 will sample 10% of the utterances)\n"
          + "  \n"
          + "  Example:\n"
          + "  java -jar speecheval.jar \\\n"
          + "    net.bertag.speecheval.clients.DeepSpeechClient http://localhost:10101/stt \\\n"
          + "    net.bertag.speecheval.data.StaedsBuilder /srv/data/steads\n"
          + "  \n"
          + "  Example:\n"
          + "  java -jar speecheval.jar \\\n"
          + "    net.bertag.speecheval.clients.KaldiClient http://localhost:10103/client/dynamic/recognize \\\n"
          + "    net.bertag.speecheval.data.TorgoBuilder /srv/data/torgo \\\n"
	  + "    0.1";
  private static final String DATASET_REFLECTION_ERROR =
      "unable to parse dataset using DATASET_BUILDER_CLASS; make sure the class defines the following"
          + " method: public static Dataset dataset(Path dataDir)";
  private static final String CLIENT_REFLECTION_ERROR =
      "unable to create client using CLIENT_CLASS; make sure the class implements SttClient";

  /**
   * Initializes a client and dataset and performs the evaluation of the ASR performance.
   *
   * @param args the arguments provided to the JVM
   * @throws IOException if the dataset cannot be read
   * @throws URISyntaxException if the ASR_URL is invalid
   * @throws IllegalArgumentException if the client and dataset cannot be constructed using the args
   */
  public static void main(String[] args)
      throws IOException, IllegalArgumentException, URISyntaxException {

    // We need at least 4 args to run the program.
    if (args.length < 4) {
      System.err.println(USAGE);
      System.exit(1);
    }

    // Initialize the client and dataset objects.
    SttClient client = clientFromClassName(args[0], new URI(args[1]));
    Dataset dataset = datasetFromClassName(args[2], Paths.get(args[3]));

    // Set the sample ratio if requested.
    if (args.length > 4) {
      dataset = dataset.samples(Double.parseDouble(args[4]));
    }

    Evaluator eval = new Evaluator(client, Scoring.ALL);
    Instant start = Instant.now();
    eval.evaluate(dataset);
    Instant end = Instant.now();

    Duration runtime = Duration.between(start, end).truncatedTo(ChronoUnit.SECONDS);
    System.out.println("Completed in " + runtime);
  }

  /**
   * Uses reflection to instantiate a dataset parser class.
   *
   * @param parserClass the fully qualified (including package) class name.
   * @param dataDir the path to the dataset on the filesystem
   * @return the dataset model
   * @throws IllegalArgumentException if the the class cannot be instantiated
   * @throws IOException if there are problesm with the given path
   */
  public static Dataset datasetFromClassName(String parserClass, Path dataDir)
      throws IllegalArgumentException, IOException {
    try {
      Class<?> parser = Class.forName(parserClass);
      return ((DatasetBuilder) parser.getConstructor().newInstance()).build(dataDir);
    } catch (ClassNotFoundException
        | InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException
        | SecurityException e) {
      throw new IllegalArgumentException(DATASET_REFLECTION_ERROR, e);
    }
  }

  /**
   * Uses reflection to instantiate a client class.
   *
   * @param clientClass the fully qualified (including package) class name.
   * @param uri the URI to the ASR service
   * @return the client model
   * @throws IllegalArgumentException if the the class cannot be instantiated
   */
  public static SttClient clientFromClassName(String clientClass, URI uri)
      throws IllegalArgumentException {
    try {
      Class<?> client = Class.forName(clientClass);
      return (SttClient) client.getConstructor(URI.class).newInstance(uri);
    } catch (ClassNotFoundException
        | InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException
        | SecurityException e) {
      throw new IllegalArgumentException(CLIENT_REFLECTION_ERROR, e);
    }
  }
}
