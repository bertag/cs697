package net.bertag.speecheval.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Optional;
import java.util.stream.Collectors;
import net.bertag.speecheval.Utils;

/**
 * DatasetBuilder that can parse the Torgo speech corpus.
 */
public class TorgoBuilder implements DatasetBuilder {
  
  public Dataset build(Path dir) throws IOException {
    return new Dataset(Files.walk(dir).filter(path -> path.toString().contains("Session"))
        .filter(path -> path.toString().contains("wav_arrayMic"))
        .filter(path -> Files.isRegularFile(path))
        .map(wavPath -> new SimpleImmutableEntry<>(wavPath, promptFor(wavPath)))
        .filter(entry -> entry.getValue().isPresent())
        .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue().orElse(""))));
  }

  private static Optional<String> promptFor(Path wavPath) {
    String id = wavPath.getFileName().toString().substring(0, 4);
    Path promptPath = wavPath.getParent().getParent().resolve("prompts").resolve(id + ".txt");

    try {
      String prompt = Files.readString(promptPath);
      return prompt.contains("[") ? Optional.empty() : Optional.of(Utils.normalize(prompt));
    } catch (IOException e) {
      return Optional.empty();
    }
  }

}
