package net.bertag.speecheval.data;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Interface defining a process for reading and parsing a speech corpus. Classes implementing this
 * interface MUST define a default public no-args constructor.
 */
public interface DatasetBuilder {

  /**
   * Builds and returns a dataset wrapping the speech corpus located at the given path.
   *
   * @param path the filesystem path to the speech corpus
   * @return the dataset for working with the speech corpus
   * @throws IOException if there are problems using the given path
   */
  public Dataset build(Path path) throws IOException;
}
