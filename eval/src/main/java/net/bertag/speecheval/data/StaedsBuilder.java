package net.bertag.speecheval.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import net.bertag.speecheval.Utils;

/**
 * DatasetBuilder that can parse the STAEDS speech corpus.
 */
public class StaedsBuilder implements DatasetBuilder {

  @Override
  public Dataset build(Path dir) throws IOException {
    Path truthFile = dir.resolve("text.txt");
    return new Dataset(
        Files.newBufferedReader(truthFile).lines().map(line -> parseTruth(dir, line))
            .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue())));
  }

  private static Entry<Path, String> parseTruth(Path baseDir, String line) {
    List<String> tokens = new LinkedList<>(Arrays.asList(line.split("\\s+")));
    Path wavPath = baseDir.resolve(tokens.remove(0));
    String transcription = Utils.normalize(String.join(" ", tokens));
    return new SimpleImmutableEntry<>(wavPath, transcription);
  }

}
