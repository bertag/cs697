package net.bertag.speecheval.data;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/** Logical wrapper for interacting with a speech corpus. */
public class Dataset {

  private final Map<Path, String> instances;

  /**
   * Constructs a new instance.
   *
   * @param instances all the utterances and their associated reference texts
   */
  public Dataset(Map<Path, String> instances) {
    this.instances = instances;
  }

  /**
   * Returns the instances in this dataset.
   *
   * @return the instances (where the key is the path to the sound file and the value is the
   *     reference text)
   */
  public Map<Path, String> instances() {
    return instances;
  }

  /**
   * Returns a random subset of this dataset as a new dataset instance.
   * 
   * @param ratio the ratio of instances which should be retained
   * @return the Dataset subset
   */
  public Dataset samples(double ratio) {
    List<Entry<Path, String>> entries = new ArrayList<>(instances.entrySet());
    Collections.shuffle(entries);

    int until = Integer.min(entries.size(), (int) Math.round(ratio * entries.size()));
    return new Dataset(
        entries
            .subList(0, until)
            .stream()
            .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue())));
  }
}
