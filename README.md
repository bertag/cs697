# CS 697

For my CS 697 project, I took early steps towards adapting existing **automatic speech recognition** (ASR) systems to perform better for people with speech disorders, particularly for dysarthric speakers.  Recognizing that this project will likely require additional work and research to be completed, I primarily focused on building a containerized, portable platform against which future researchers could develop.  The following document describes the various components of this platform and how to use them.

## Servers

For this project, I tried to exclusively setup and use docker-based server images which could perform ASR via web services.  There are several server setups that proved useful.  They are described in detail below, but there are also some helper scripts to quickly start/stop all of them at once:

- `servers/run-all.sh` will build all the server images and run them all as Docker containers.
- `servers/stop-all.sh` will stop all the docker containers started in the previous script.

### Deep Speech (HTTP)

This server setup is based on [Mozilla's Deep Speech implementation](https://github.com/mozilla/DeepSpeech).  Users POST a .wav file to the web service, and the HTTP response provides the predicted transcription.

To build the image, run:

```bash
docker build -t bertag/deepspeech-http servers/deepspeech-http
```

To start a container based on the resulting image, run:

```bash
docker run --name deepspeech-http --publish 10101:8080 bertag/deepspeech-http:latest
```

Once the container is started, the speech-to-text service is published to `http://localhost:10101/stt`.  The following CURL command will send a .wav file to the server:

```bash
curl --data-binary @'/path/to/audio/file.wav' http://localhost:10101/stt --verbose
```

### Deep Speech (WebSocket)

This server is also based on Mozilla's Deep Speech, but is used to provide real-time transcription of streaming audio through the use of WebSockets.

To build the image, run:

```bash
docker build -t bertag/deepspeech-ws servers/deepspeech-ws
```

To start a container based on the resulting image, run:

```bash
docker run --name deepspeech-ws --publish 10102:8080 bertag/deepspeech-ws:latest
```

Once the container is started, the speech-to-text service is published to `ws://localhost:10102/recognize`.  You will need a websocket-capable client to send streaming audio to the server.  This image is based on a [websocket server written by David Zurow](https://github.com/daanzu/deepspeech-websocket-server), which also shipped with a command-line Python client.  You can install the client by running `clients/install-deepspeech-client.sh`.  Once installed, run the client by calling:

```bash
python clients/deepspeech/client.py --aggressiveness 0 --server ws://localhost:10102/recognize
```

The aggressiveness value determines how sensitive the client is to detecting utterance breaks.  By default it is set to the max value (3), but I found lower values worked better and led to more coherant utterances.  This may need further tweaking based on your specific microphone setup and speech patterns.

### Kaldi (HTTP or WebSocket)

This server setup is based on the venerable [Kaldi ASR system](https://kaldi-asr.org/), which has [been extended by other developers](https://github.com/alumae/kaldi-gstreamer-server) to provide a [gstreamer](https://gstreamer.freedesktop.org/)-based wrapper for making speech recognition available using a client-server model.  While the instructions below setup a single server, it should be noted that the [underlying server](https://github.com/jcsilva/docker-kaldi-gstreamer-server) is designed to be distributed across multiple workers, allowing great performance flexibility.

To build the image, run:

```bash
docker build -t bertag/kaldi servers/kaldi
```

To start a container based on the resulting image, run:

```bash
docker run --name kaldi --rm --detach --publish 10103:80 bertag/kaldi:latest
```

Once the container is started, the speech-to-text service is available for HTTP/RESTful and WebSocket use.  The HTTP service is available at `http://localhost:10103/client/dynamic/recognize`.  The following CURL command will send a .wav file to the server.

```bash
curl -XPUT --data-binary @'/path/to/audio/file.wav' http://localhost:10103/client/dynamic/recognize --verbose
```

The websocket service is available at `ws://localhost:10103/client/ws/speech`.  As with the WebSocket Deep Speech server, you will need a websocket-capable client to interact with it.  Zurow's previously mentioned Python client by can be quickly modified to do the job.  To install and modify the client, run `clients/install-kaldi-client.sh`.  Once installed, run the client by calling:

```bash
python clients/kaldi/client.py --aggressiveness 0 --server ws://localhost:10103/client/ws/speech
```

## Speech Corpora

I primarily used 2 speech corpora for this project, but others can be supported with minimal additional development.

### STAEDS

The **ST-AEDS-20180100_1, Free ST American English Corpus**, referred to subsequently as "STAEDS", consists of 10 speakers, with ~350 utterances for each.  See http://www.openslr.org/45/ for full details including licensing.

Run `corpora/download_staeds.sh` to download the corpus into `corpora/staeds`.

### TORGO

The **TORGO database of acoustic and articulatory speech from speakers with dysarthria**, referred to subsequently as "TORGO", consists of 15 speakers (8 control speakers and 7 dysarthric speakers), with a variable number of utterances for each.  See http://www.cs.toronto.edu/~complingweb/data/TORGO/torgo.html for full details including licensing.

Run `corpora/download_torgo.sh` to download the corpus into `corpora/torgo`.

## Evaluation Framework

I also developed a custom evaluation program to facilitate rapid testing of different setups.  This program is written for Java 11 and is available in the `eval` directory of this repository.  To build the image, run:

```bash
docker build -t bertag/speecheval eval
```

This docker image is a little more complex than the others.  For starters, we will [mount a directory](https://docs.docker.com/storage/volumes/) containing the corpus we are evaluating using the `--volume` option in `docker run`.  We must also [enable networking for the container](https://docs.docker.com/network/) so that it can communicate with the server.  We must then specify some criteria for the evaluation as positional arguments to Docker.  The positional arguments are:

1. The fully-qualified Java Class which acts as a client to the ASR server (out of the box, `net.bertag.speecheval.clients.DeepSpeechClient` and `net.bertag.speecheval.clients.KaldiClient` are included, and others may be added to the image as needed).
2. The URL of the HTTP speech-to-text endpoint for the ASR server.  The necessary endpoints for the Deep Speech (HTTP) and Kaldi servers are defined in their respective sections above.
3. The fully-qualified Java Class which facades/parses the corpus used for evaluation (out of the box, `net.bertag.speecheval.data.StaedsBuilder` and `net.bertag.speeceval.data.TorgoBuilder` are included, and others may be added as needed).
4. The file path in the container where the corpus resides (this will match the right-hand side of the `--volume` optflag).
5. [Optional] The ratio of the corpus to use (e.g.: to use 15% of the corpus, enter "0.15").  Utterances will be randomly sampled from the full corpus.

Consider the following example, which:

- Uses the "host" network (suitable for development and research, but probably not for production), and
- Mounts the STAEDS corpus, to
- Evaluate a DeepSpeech (HTTP) server running at http://localhost:10101
- Against 1% of the corpus.

```bash
docker run \
  --rm \
  --network host \
  --volume corpora/staeds:/srv/data/steads \
  bertag/speecheval \
  net.bertag.speecheval.clients.DeepSpeechClient http://localhost:10101/stt \
  net.bertag.speecheval.data.StaedsBuilder /srv/data/steads \
  0.01
```

### Extending the Evaluation Framework

Recognizing that I have only scratched the surface of possibilities regarding this project, I have strived to make the evaluation program easy to extend.  Support for additional ASR systems, speech corpora, and scoring metrics are all possible.

To add support for **additional ASR systems**, simply implement the `net.bertag.speecheval.clients.SttClient` interface.  Your implementation should call the speech-to-text (STT) service of the ASR system and return the predicted transcription of the provided utterance as a String.  Note that there is an additional requirement to define a constructor which accepts a `java.net.URI` object as its sole parameter.  Java 11 provides good tooling for making web calls out-of-the-box, but since each ASR system will structure response data differently, you will need to adapt your client to isolate and return the hypothesis string from a potentially complex response.

To add support for **additional speech corpora**, implement the `net.bertag.speecheval.data.DatasetBuilder` interface.  The sole method in this interface accepts a `java.nio.file.Path` object and returns a `net.bertag.speecheval.data.Dataset` wrapper for the corpus.  There is an additional requirement to define a public default (no-args) constructor.  The key commonality of all `DatasetBuilder` implementations is that they define a Map or dictionary such that the key of the map is the path to a sound file containing an utterance, and the value is the reference ("ground truth") transcription of that utterance.  Since each corpus will be wildly different in its directory layout and how reference transcriptions are stored, each `DatasetBuilder` will be very unique.  But ultimately, it boils down to isolating and parsing out those two key pieces of information for each utterance/instance.

No common interface is defined for scoring.  To add **additional scorings mechanisms**, modify the `net.bertag.speecheval.eval.Scoring` class to statically define your new algorithm as as a `DoubleBiFunction<String, String>`.  In other words, define a method that accepts two strings (the first being the reference or "ground truth" string, and the second being the prediction or hypothesis string) and returns a double (the score).  Be sure to register your method to the constant `Scoring.ALL` expression so that it gets used in the evaluation loop.

Feel free to contact me if you have questions about extending the evaluation framework, and please submit merge requests back to the project if you do end up extending it, so that others may benefit from your work as well!

## Other Project Notes

- [OpenSLR](http://openslr.org) provides a number of open-source speech corpora.
- While time did not allow for any work with this dataset, I had hoped to play with the [CSTR VCTK corpus](http://homepages.inf.ed.ac.uk/jyamagis/page3/page58/page58.html), which features different accents from the UK.  While not directly related to dysarthric speech, accent recognition seems like an obvious place where models could be made more robust, which could yield improvements in the core problem of the project as well.
- I relied upon pre-trained models included with the different servers used in the project.  However, we found their accuracy to be pretty dismal.  One of the next steps in this project should be to find better models or perform additional training on the pre-built models.
- Kaldi proved to be a major bear in this project.  I'm not sure if I set it up weird, or if the model was just weak, or what...but I spent an inordinate amount of time just trying to keep Kaldi running and stable.  It mostly just wanted to crash or hang up on me.  And that is a shame, because it is a truly venerable system, and others have had great success with it.
- I looked at both "offline" (submitting an existing sound file) and "online/streaming" (transcribing utterances as they are being spoken) approaches to speech recognition.  Offline is more stable and intuitively seems to offer the ASR systems better context, but streaming audio will prove to be more useful for any real-world system.
- Early on, I tried to get CUDA going with the Docker servers.  It seems to be possible, with Nvidia even offering a Docker plugin to make the GPU available to the container.  However, I wound up in version hell as I tried to balance OS/driver dependencies with Python versions and ML framework (like tensorflow) versions.  I almost cracked it by building my own Deep Speech image from the bottom up, rather than starting with a partially-setup image, but other aspects of the project required my attention and I didn't have time to figure it out later.
- The [Kõnele](https://github.com/Kaljurand/K6nele) app [for Android](https://play.google.com/store/apps/details?id=ee.ioc.phon.android.speak&hl=en_US) can be used as a mobile client for the Kaldi service.  I got it connected and it recognizes the server, but the transcription service doesn't seem to respond.  I'm still debugging that.

---------------------

Project Icon: https://commons.wikimedia.org/wiki/File:Circle-icons-mic.svg (Licensed GPL2+, originally created by "Elegant Themes": https://archive.is/Uc2Ni)
